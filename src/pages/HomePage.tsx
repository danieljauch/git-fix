import React, { Component } from 'react';

import { Page } from "../containers";

export default class HomePage extends Component {
  render() {
    return <Page />;
  }
}
