import React, { Component } from 'react';

import Header from './Header';
import Footer from './Footer';

export default class Page extends Component {
	render() {
		const { children } = this.props;

		return (
			<div className="page__container">
				<Header />
				<main className="page__main">{children}</main>
				<Footer />
			</div>
		);
	}
}
