import React, { Component } from 'react';

export default class Nav extends Component {
  render() {
		const { children } = this.props;

    return (
      <nav className="navigation__container">
        <ul className="navigation__list">{children}</ul>
      </nav>
    )
  }
}
