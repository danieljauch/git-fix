import React, { Component } from 'react';

export default class BreadCrumbs extends Component {
  render() {
		const { children } = this.props;

    return (
      <nav className="breadcrumb__container">
        <ul className="breadcrumb__list">{children}</ul>
      </nav>
    )
  }
}
