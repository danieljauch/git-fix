export { default as BreadCrumbs } from "./BreadCrumbs";
export { default as Footer } from "./Footer";
export { default as Header } from "./Header";
export { default as Nav } from "./Nav";
export { default as Page } from "./Page";
