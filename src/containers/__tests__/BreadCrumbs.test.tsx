import React from "react";
import renderer from "react-test-renderer";

import BreadCrumbs from "../BreadCrumbs";

describe("BreadCrumbs", () => {
  it("matches snapshot", () => {
    const tree = renderer.create(<BreadCrumbs />).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
