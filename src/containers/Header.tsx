import React, { Component } from 'react';

import Nav from './Nav';
import { NavItem } from '../elements';

export default class Header extends Component {
	render() {
		return (
			<header className="app__header">
				<Nav>
					<NavItem path="/">Home</NavItem>
				</Nav>
			</header>
		);
	}
}
