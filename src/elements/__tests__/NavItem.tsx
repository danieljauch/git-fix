import React from "react";
import renderer from "react-test-renderer";

import NavItem from "../NavItem";

describe("NavItem", () => {
  it("matches snapshot", () => {
    const tree = renderer.create(<NavItem path="/" />).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
