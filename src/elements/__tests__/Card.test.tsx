import React from "react";
import renderer from "react-test-renderer";

import Card from "../Card";

describe("Card", () => {
  it("matches snapshot", () => {
    const tree = renderer.create(<Card />).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
