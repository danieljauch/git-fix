import React from "react";
import renderer from "react-test-renderer";

import BreadCrumb from "../BreadCrumb";

describe("BreadCrumb", () => {
  it("matches snapshot", () => {
    const tree = renderer.create(<BreadCrumb to="/" />).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
