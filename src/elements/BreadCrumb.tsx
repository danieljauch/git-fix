import React, { Component } from 'react';
import { Link } from 'react-router-dom';

type Props = {
	path: string;
};

export default class BreadCrumb extends Component<Props> {
	render() {
		const { children, path } = this.props;

		return (
			<li className="breadcrumb__item">
				<Link className="breadcrumb__link" to={path}>{children}</Link>
			</li>
		);
	}
}
