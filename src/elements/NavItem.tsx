import React, { Component } from 'react';
import { Link } from 'react-router-dom';

type Props = {
	path: string;
};

export default class NavItem extends Component<Props> {
	render() {
		const { children, path } = this.props;

		return (
			<li className="navigation__item">
				<Link className="navigation__link" to={path}>{children}</Link>
			</li>
		);
	}
}
