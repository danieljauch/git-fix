import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import "./assets/scss/style.scss";

import { HomePage } from "./pages";

export default class App extends Component {
	render() {
		return (
			<div className="app__container">
				<Router>
					<Route path="/" component={HomePage} />
				</Router>
			</div>
		);
	}
}
